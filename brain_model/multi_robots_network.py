# -*- coding: utf-8 -*-
"""
This is a minimal brain with 2 neurons connected together.
"""
from __future__ import division
from past.utils import old_div
# pragma: no cover

__author__ = 'Template'

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np

def create_brain():
    """
    Initializes PyNN with the minimal neuronal network
    """

    sim.setup(timestep=0.1, min_delay=0.1, max_delay=20.0, threads=1, rng_seeds=[1234])

    # Following parameters were taken from the husky braitenberg brain experiment (braitenberg.py)

    SENSORPARAMS = {'cm': 0.025,
                    'v_rest': -60.5,
                    'tau_m': 10.,
                    'e_rev_E': 0.0,
                    'e_rev_I': -75.0,
                    'v_reset': -60.5,
                    'v_thresh': -60.0,
                    'tau_refrac': 10.0,
                    'tau_syn_E': 2.5,
                    'tau_syn_I': 2.5}

    SYNAPSE_PARAMS = {"weight": 0.5e-4,
                      "delay": 20.0,
                      'U': 1.0,
                      'tau_rec': 1.0,
                      'tau_facil': 1.0}

    cell_class = sim.IF_cond_alpha(**SENSORPARAMS)

    # Define the network structure: 2 times 2 neurons (1 sensor and 1 actor per population)
    population = sim.Population(size=4 + 45 + 45 + 4 + 45 + 45 + 4,
                                cellclass=cell_class)

    icub_in_1 = slice(0, 1)
    icub_in_2 = slice(1, 2)
    icub_out_1 = slice(2, 3)
    icub_out_2 = slice(3, 4)

    LASER_SIZE = 45

    p_green_laser_on = slice(icub_out_2.stop, icub_out_2.stop + LASER_SIZE)
    # First half of p_green_laser_on
    p_green_laser_on_left = slice(p_green_laser_on.start, p_green_laser_on.start + old_div((p_green_laser_on.stop - p_green_laser_on.start), 2))
    # Second half of p_green_laser_on
    p_green_laser_on_right = slice(p_green_laser_on_left.stop, p_green_laser_on.stop)

    p_green_laser_off = slice(p_green_laser_on.stop, p_green_laser_on.stop + LASER_SIZE)
    # First half of p_green_laser_off
    p_green_laser_off_left = slice(p_green_laser_off.start, p_green_laser_off.start + old_div((p_green_laser_off.stop - p_green_laser_off.start), 2))
    # Second half of p_green_laser_off
    p_green_laser_off_right = slice(p_green_laser_off_left.stop, p_green_laser_off.stop)

    p_green_move_forward = slice(p_green_laser_off.stop, p_green_laser_off.stop + 1)
    p_green_move_backward = slice(p_green_move_forward.stop, p_green_move_forward.stop + 1)
    p_green_move_left = slice(p_green_move_backward.stop, p_green_move_backward.stop + 1)
    p_green_move_right = slice(p_green_move_left.stop, p_green_move_left.stop + 1)

    p_red_laser_on = slice(p_green_move_right.stop, p_green_move_right.stop + LASER_SIZE)
    p_red_laser_off = slice(p_red_laser_on.stop, p_red_laser_on.stop + LASER_SIZE)

    # First half of p_red_laser_on
    p_red_laser_on_left = slice(p_red_laser_on.start, p_red_laser_on.start + old_div((p_red_laser_on.stop - p_red_laser_on.start), 2))
    # Second half of p_red_laser_on
    p_red_laser_on_right = slice(p_red_laser_on_left.stop, p_red_laser_on.stop)

    # First half of p_red_laser_off
    p_red_laser_off_left = slice(p_red_laser_off.start, p_red_laser_off.start + old_div((p_red_laser_off.stop - p_red_laser_off.start), 2))
    # Second half of p_red_laser_off
    p_red_laser_off_right = slice(p_red_laser_off_left.stop, p_red_laser_off.stop)

    p_red_move_forward = slice(p_red_laser_off.stop, p_red_laser_off.stop + 1)
    p_red_move_backward = slice(p_red_move_forward.stop, p_red_move_forward.stop + 1)
    p_red_move_left = slice(p_red_move_backward.stop, p_red_move_backward.stop + 1)
    p_red_move_right = slice(p_red_move_left.stop, p_red_move_left.stop + 1)

    # bias_green = sim.DCSource(amplitude=0.5, start=0.0, stop=float('inf'))
    # bias_red = sim.DCSource(amplitude=0.5, start=0.0, stop=float('inf'))

    synapse_type = sim.TsodyksMarkramSynapse(**SYNAPSE_PARAMS)
    connector = sim.AllToAllConnector()

    # Connect neurons
    sim.Projection(presynaptic_population=population[icub_in_1],
                   postsynaptic_population=population[icub_out_1],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    sim.Projection(presynaptic_population=population[icub_in_2],
                   postsynaptic_population=population[icub_out_2],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    ##  RED PIONEER
    # Connecting the "go forward" neuron
    sim.Projection(presynaptic_population=population[p_red_laser_off],
                   postsynaptic_population=population[p_red_move_forward],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    # Connecting the "move backward" neuron
    sim.Projection(presynaptic_population=population[p_red_laser_on],
                   postsynaptic_population=population[p_red_move_backward],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    # Connecting the "turn right neuron"
    sim.Projection(presynaptic_population=population[p_red_laser_on_left],
                   postsynaptic_population=population[p_red_move_right],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    # Connecting the "turn left neuron"
    sim.Projection(presynaptic_population=population[p_red_laser_on_right],
                   postsynaptic_population=population[p_red_move_left],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    ## GREEN PIONEER
    # Connecting the "go forward" neuron
    sim.Projection(presynaptic_population=population[p_green_laser_off],
                   postsynaptic_population=population[p_green_move_forward],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    # Connecting the "just turn around" neuron
    sim.Projection(presynaptic_population=population[p_green_laser_on],
                   postsynaptic_population=population[p_green_move_backward],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    # Connecting the "turn right neuron"
    sim.Projection(presynaptic_population=population[p_green_laser_on_left],
                   postsynaptic_population=population[p_green_move_right],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    # Connecting the "turn left neuron"
    sim.Projection(presynaptic_population=population[p_green_laser_on_right],
                   postsynaptic_population=population[p_green_move_left],
                   connector=connector,
                   synapse_type=synapse_type,
                   receptor_type='excitatory')

    sim.initialize(population, v=population.get('v_rest'))

    return population

circuit = create_brain()
