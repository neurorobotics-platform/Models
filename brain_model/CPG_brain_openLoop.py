# -*- coding: utf-8 -*-
"""
Loads a Spiking Network described in a SNN file

"""
from builtins import range
# pragma: no cover

__author__ = 'Alexander_UGent'

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np
import logging
import pickle
import os

SNN_filename = os.path.join(os.environ.get('NRP_MODELS_DIRECTORY'), 'tigrillo/SNNfiles/cmaes_solution.SNN')
logger = logging.getLogger(__name__)


def loadSNNFromFile(filename):
    with open(filename, 'r') as f:
        NetworkData = pickle.load(f)
    return NetworkData

NetworkData = loadSNNFromFile(SNN_filename)

networkStructure = NetworkData['networkStructure']
NexcNinh = NetworkData['N_excN_inh']
weights = NetworkData['weights']
Pconnect = NetworkData['Pconnect']

timestep=0.5
sim.setup(timestep=timestep, min_delay=0.5, max_delay=1001.0, threads=1, rng_seeds=[1234])

neuron_parameters = {
    'cm': 0.2,
    'v_reset': -75,
    'v_rest': -65,
    'v_thresh': -50,
    'tau_m': 30.0,  # sim.RandomDistribution('uniform', (10.0, 15.0)),
    'tau_refrac': 2.0,
    'tau_syn_E': 0.5,  # np.linspace(0.1, 20, hiddenP_size),
    'tau_syn_I': 0.5,
    'i_offset': 0.0
}

readout_parameters = {
    'cm': 0.2,
    'v_reset': -75,
    'v_rest': -65,
    'v_thresh': 5000,
    'tau_m': 30.0,  # sim.RandomDistribution('uniform', (10.0, 15.0)),
    'tau_refrac': 0.1,
    'tau_syn_E': 5.5,  # np.linspace(0.1, 20, hiddenP_size),
    'tau_syn_I': 5.5,
    'i_offset': 0.0
}

# =================================================================================================================
# create connectors
# =================================================================================================================
connector_hiddenPexc_hiddenPinh = sim.FixedProbabilityConnector(p_connect=Pconnect[2])
connector_hiddenPinh_hiddenPexc = sim.FixedProbabilityConnector(p_connect=Pconnect[2])
# =================================================================================================================
# create populations
# =================================================================================================================
hiddenPexc_size, hiddenPinh_size = NexcNinh[0], NexcNinh[1]
N_input, N_res, N_readouts = networkStructure[0], networkStructure[1], networkStructure[2]

input_populations = []
for idx in range(N_input):
    input_populations.append(sim.Population(hiddenPexc_size, sim.IF_curr_exp, neuron_parameters))

hidden_populations = [[] for x in range(N_res)]
for idx in range(N_res):
    hiddenPexc = sim.Population(hiddenPexc_size, sim.IF_curr_exp, neuron_parameters)
    hiddenPinh = sim.Population(hiddenPinh_size, sim.IF_curr_exp, neuron_parameters)
    hidden_populations[idx].append(hiddenPexc)
    hidden_populations[idx].append(hiddenPinh)

readout_neuron_populations = []
for i in range(N_readouts):
    pop = sim.Population(1, sim.IF_curr_exp, readout_parameters)
    readout_neuron_populations.append(pop)

# =================================================================================================================
# create projections
# =================================================================================================================
# create interpopulation projections
input_weights, res_weights, readout_weights = weights[0], weights[1], weights[2]
Pconnect_input, Pconnect_inter, Pconnect_intra = Pconnect[0], Pconnect[1], Pconnect[2]

fraction_inverted = 0.5  # cave, hard coded parameter
N_res = networkStructure[1]
N_readouts = networkStructure[2]
N_uninverted = int(N_res - N_res * fraction_inverted)

projections_inp_hiddenP = []
for idx, pop in enumerate(hidden_populations[:N_uninverted]):
    projection_inp_hiddenP = sim.Projection(input_populations[0], pop[0],
                                            sim.FixedProbabilityConnector(p_connect=Pconnect_input[idx]),
                                            sim.StaticSynapse(weight=input_weights[idx, 0]))  
    projections_inp_hiddenP.append(projection_inp_hiddenP)  

for idx, pop in enumerate(hidden_populations[N_uninverted:]):
    projection_inp_hiddenP = sim.Projection(input_populations[1], pop[0], sim.FixedProbabilityConnector(
        p_connect=Pconnect_input[idx + N_uninverted]),
                                            sim.StaticSynapse(weight=input_weights[idx + N_uninverted, 0]))  
    projections_inp_hiddenP.append(projection_inp_hiddenP)  

projections_hiddenP_readout = []
connector = sim.AllToAllConnector()  
for idx0 in range(N_readouts):
    readout_pop = readout_neuron_populations[idx0]    
    projections = []
    for idx1 in range(N_res):
        pop0exc = hidden_populations[idx1][0]
        weight = readout_weights[idx0][idx1, 0]

        projection = sim.Projection(pop0exc, readout_pop, connector,
                                    sim.StaticSynapse(weight=weight))
        projections.append(projection)
    projections_hiddenP_readout.append(projections)

rng = sim.NumpyRNG(seed=2007200)
projections_hiddenP_hiddenP = []
for idx0 in range(N_res):
    pop0exc = hidden_populations[idx0][0]
    for idx1 in range(N_res):
        pop1exc = hidden_populations[idx1][0]
        projection = sim.Projection(pop0exc, pop1exc,
                                    sim.FixedProbabilityConnector(p_connect=Pconnect_inter[idx1, idx0], rng=rng),
                                    sim.StaticSynapse(weight=res_weights[idx1, idx0],
                                                      delay=np.random.randint(4, 8)))
        projections_hiddenP_hiddenP.append(projection)

# create intrapopulation projections
for idx in range(N_res):
    hiddenPexc = hidden_populations[idx][0]
    hiddenPinh = hidden_populations[idx][1]
    projection_hiddenPexc_hiddenPinh = sim.Projection(hiddenPexc, hiddenPinh, connector_hiddenPexc_hiddenPinh,
                                                      sim.StaticSynapse(weight=1.0))
    # projection_hiddenPexc_hiddenPexc = sim.Projection(hiddenPexc, hiddenPexc, connector_hiddenPexc_hiddenPexc,
    #                                                   sim.StaticSynapse(weight=0.1))
    projection_hiddenPinh_hiddenPexc = sim.Projection(hiddenPinh, hiddenPexc, connector_hiddenPinh_hiddenPexc,
                                                      sim.StaticSynapse(weight=-1.0))
    # projection_hiddenPinh_hiddenPinh = sim.Projection(hiddenPinh, hiddenPinh, connector_hiddenPinh_hiddenPinh,
    #                                                   sim.StaticSynapse(weight=-0.1))

# =================================================================================================================
# inject current and set recordings
# =================================================================================================================
# create and inject noise
white_noise_input = sim.NoisyCurrentSource(mean=0.0, stdev=2.0, start=0.0, dt=timestep)
input_populations[0].inject(white_noise_input)
input_populations[1].inject(white_noise_input)

# create and inject sine
sine = sim.ACSource(start=0.0, amplitude=1.5, offset=1.5, frequency=1.44, phase=150.0)
sine.inject_into(input_populations[0])
# create and inject another sine
sine2 = sim.ACSource(start=0.0, amplitude=1.5, offset=1.5, frequency=1.44, phase=60.0)
sine2.inject_into(input_populations[1])

# set recordings
for pop in readout_neuron_populations:
	pop.record(['v'], sampling_interval=5.0) #sample every 5 ms

# return circuit
circuit = sim.Population(1, sim.IF_curr_exp, readout_parameters) # return dummy population




