# HoLLiE description files

# Authors
camilo, tristan

## Contents

Contains a .urdf file and all required meshes for the HoLLiE robot. No other packages are neccessary.


## Installation
For usage with Gazebo: 
 - Clone into catkin workspace
 - catkin_make

 For usage with the NRP:
 - Clone into Models folder
 - add "hollie_robot" in $HBP/Models/_rpmbuild/models.txt
 - Rerun create_symlinks in /Models
 
 - Alternative: manually checkout/copy robot into .gazebo/Models


## Usage
Import/spawn mmmrobot.urdf in Gazebo.
 - E.g. export GAZEBO_MODEL_PATH=~/catkin_ws/src/

Publish to the rostopics of the controllers to move the robot.

## Sources

- [1] https://ids-git.fzi.de/ros/schunk_description - arm meshes
- [2] https://ids-git.fzi.de/ros/schunk_svh_driver - hand meshes
- [3] https://ids-git.fzi.de/manipulation/ros_hollie - hollie .xacro

## Model creation

The following steps were taken to create the .urdf model and have to be repeated if the original .xacro changes:

- clone [1] and [2] into the catkin workspace
- build .urdf from xacro in [3]
- import plain .urdf into RobotDesigner
- export as ros package
(if you cannot load the robot in gazebo by this point, replace /src/hollie_robot/ with /hollie_robot/ in the urdf)
- check robot and manually fix problems created by robot designer output
- calibrate joints/copy values from previous version