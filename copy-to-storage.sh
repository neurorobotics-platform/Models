#!/bin/bash
# Author: Stefano Nardo
#
# This script copies the production models to the storage. After having run 
# it, remember to update the DB by running the corresponding script in 
# nrpBackendProxy (npm run update_template_models).

#check if the STORAGE_PATH is set, if not we use the default ~/.opt/nrpStorage
if [ -z ${STORAGE_PATH+x} ];
 then echo "STORAGE_PATH is unset. Defaulting to ~/.opt/nrpStorage"
  STORAGE_PATH=~/.opt/nrpStorage
 else echo "STORAGE_PATH is set to '$STORAGE_PATH'"
fi
#check if the storage path exists
if [ ! -d "$STORAGE_PATH" ]; then
 mkdir -p $STORAGE_PATH || { echo ERROR; exit 1;}
fi
cd $STORAGE_PATH
rm -rf TEMPLATE_MODELS
mkdir TEMPLATE_MODELS
cd TEMPLATE_MODELS
cp -r $HBP/Models/brains .
cp -Lr $HBP/Models/environments .
cp -Lr $HBP/Models/robots .

